__author__ = 'Petar'

import pysynth_b as pys
import numpy as np
import random
import os

"""
Notes:
------

- Create functions for chords. These will require the mix_files function to stack all notes in a chord
- Creating nice sounding melodies might be achieved by having some sort of next-suitable-note tree which
could be created by researching chords and music theory.
"""
# when making proper music generator func use long duration (remember 0.1 is longer than 10)
# for low (bass) notes

class MusicGenerator:

    def __init__(self):
        os.chdir('C:\Users\Petar\Google Drive\My Projects\Music Generator')
        letters = []
        numbers = []
        with open('notes.txt', 'r') as f:
            for line in f:
                item = line.replace(',', '')
                letters.append(item.split(': ')[0].replace("'", ""))
                numbers.append(int(item.split(': ')[1].replace('\n', '')))
            self.notes = dict(zip(numbers, letters))
        os.chdir('C:\Users\Petar\Google Drive\My Projects\Music Generator\Songs')
        self.song = []
        self.length = 0
        self.filename = ''
    def instructions(self):
        """
        Prints out the list of music notes and other details about the pysynth module.
        Returns
        -------
            None
        """

        os.chdir('C:\Users\Petar\Google Drive\My Projects\Music Generator')
        with open('Instructions.txt', 'r') as f:
            read_instruction = f.read()
            print read_instruction
        os.chdir('C:\Users\Petar\Google Drive\My Projects\Music Generator\Songs')

    def __choose_next_note(self, possible_notes):
        chosen_base_note = random.choice(possible_notes)
        duration = np.random.randint(2, 4)
        octave = 5
        next_note = chosen_base_note + str(octave)
        #print next_note, "  ", duration

        self.song.append((next_note, duration))
        self.__predict_note(chosen_base_note)

    def __predict_note(self, base_note_seed):
        self.length += 1
        if self.length < 100:
            if base_note_seed == 'c':
                #possible_notes = ['a', 'a#', 'd', 'd#']
                possible_notes = ['c', 'd', 'd#']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'c#':
                #possible_notes = ['a#', 'b', 'd#', 'e']
                possible_notes = ['c#', 'd#', 'e']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'd':
                #possible_notes = ['c', 'b', 'e', 'f']
                possible_notes = ['d', 'e', 'f']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'd#':
                #possible_notes = ['c', 'c#', 'f', 'f#']
                possible_notes = ['d#', 'f', 'f#']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'e':
                #possible_notes = ['d', 'c#', 'f#', 'g']
                possible_notes = ['e', 'f#', 'g']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'f':
                #possible_notes = ['d', 'd#', 'g', 'g#']
                possible_notes = ['f', 'g', 'g#']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'f#':
                #possible_notes = ['d#', 'e', 'g#', 'a']
                possible_notes = ['f#', 'g#', 'a']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'g':
                #possible_notes = ['e', 'f', 'a', 'a#']
                possible_notes = ['g', 'g#', 'a']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'g#':
                #possible_notes = ['f', 'f#', 'a#', 'b']
                possible_notes = ['g#', 'a#', 'b']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'a':
                #possible_notes = ['f#', 'g', 'b', 'b']
                possible_notes = ['a', 'b', 'b']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'a#':
                #possible_notes = ['g', 'g#', 'c', 'c#']
                possible_notes = ['a#', 'c', 'c#']
                self.__choose_next_note(possible_notes)
            elif base_note_seed == 'b':
                #possible_notes = ['g#', 'a', 'c#', 'd']
                possible_notes = ['b', 'c#', 'd']
                self.__choose_next_note(possible_notes)
        else:
            self.__create_song(self.song, self.filename)

    def __create_song(self, song, filename):
        """
        Used as a general method to create the .wav files.

        Returns
        -------
            None
        """

        pys.make_wav(song, fn=filename)

    def make_melody(self, base_note_seed, filename):
        del self.song[:]
        self.length = 0
        self.filename = filename
        self.__predict_note(base_note_seed)

"""
        #Note: Sharp notes have been excluded (at this moment)
        self.notes = {0: 'a0',   1: 'b0',   2: 'c1',   3: 'c#1',  4: 'd1',   5: 'e1',   6: 'f1',   7: 'g1',
                      8: 'g#1',  9: 'a1',   10: 'a#1', 11: 'b1',  12: 'c2',  13: 'c#2', 14: 'd2',  15: 'd#2',
                      16: 'e2',  17: 'f2',  18: 'f#2', 19: 'g2',  20: 'g#2', 21: 'a2',  22: 'a#2', 23: 'b2',
                      24: 'c3',  25: 'c#3', 26: 'd3',  27: 'd#3', 28: 'e3',  29: 'f3',  30: 'f#3', 31: 'g3',
                      32: 'g#3', 33: 'a3',  34: 'a#3', 35: 'b3',  36: 'c4',  37: 'c#4', 38: 'd4',  39: 'd#4',
                      40: 'e4',  41: 'f4',  42: 'f#4', 43: 'g4',  44: 'g#4', 45: 'a4',  46: 'a#4', 47: 'b4',
                      48: 'c5',  49: 'c#5', 50: 'd5',  51: 'd#5', 52: 'e5',  53: 'f5',  54: 'f#5', 55: 'g5',
                      56: 'g#5', 57: 'a5',  58: 'a#5', 59: 'b5',  60: 'c6',  61: 'c#6', 62: 'd6',  63: 'd#6',
                      64: 'e6',  65: 'f6',  66: 'f#6', 67: 'g6',  68: 'g#6', 69: 'a6',  70: 'a#6', 71: 'b6',
                      72: 'c7',  73: 'c#7', 74: 'd7',  75: 'd#7', 76: 'e7',  77: 'f7',  78: 'f#7', 79: 'g7',
                      80: 'g#7', 81: 'a7',  82: 'a#7', 83: 'b7',  84: 'c8',  85: 'a#0', 86: 'd#1', 87: 'f#1'}

"""